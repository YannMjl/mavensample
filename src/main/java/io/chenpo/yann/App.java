package io.chenpo.yann;

import io.cloudrepo.artifactA.HelloWorld;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // create instance for class hello word
        HelloWorld helloWorld = new HelloWorld();

        // call say hello function
        helloWorld.sayHello();

        System.out.println( "Hello World!" );
    }
}
